/* Validaciones para el registro */
$(document).ready(function (){
    $("#Registrar").validate({
        errorClass: 'is-invalid',
        rules:{
            Nickname:{
                required: true,
            },
            contrasena:{
                required: true,
            },
            nombre:{
                required: true,
            },
            apellido:{
                required: true,
            },
            correo:{
                required: true,
                email: true
            },
            fechanac:{
                required: true
            }
        },
        messages:{
            Nickname:{
                required:"Se debe ingresar un nombre de usuario."
            },
            nombre:{
                required:"Se debe ingresar el nombre."
            },
            apellido:{
                required:"Se debe ingresar el apellido."
            },
            correo:{
                required: "Se debe ingresar un correo electrónico.",
                email: "Debe ingresar un correo válido."
            },
            contrasena:{
                required: "Se debe ingresar una contraseña."
            },
            fechanac:{
                required: "Se debe ingresar una fecha de nacimiento."
            }
        }
    })
})

$("#formulario").submit(function(){
    if($("#formulario").valid()){
        return true
        }else{
        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Faltan campos por llenar.',
          })
    }
    return false;
})

/* Validaciones para el Login */
$(document).ready(function() {
    $("#login").validate({
        errorClass: 'is-invalid',
        rules:{
            nombreuser:{
                required: true
            },
            contrasena:{
                required: true
            },
        },
        messages:{
            nombre:{
                required:"Ingresa un nombre de usuario."
            },
            contrasena:{
                required:"Ingresa una contraseña."
            }
        }
        
    })
})

$("#login").submit(function(){
    if($("#login").valid()){
        return true;
    }else{
         Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Faltan campos por llenar.',
            footer: '<a href>¿Olvidaste tu contraseña?</a>'
        })
    }
    return false
})


