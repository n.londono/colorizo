from django.contrib import admin
from django.urls import path
from .views import welcome
from .views import login
from .views import registro
from .views import home
from .views import logout
from .views import recuperacion

urlpatterns =[
    path('',welcome),
    path('login',login, name='login'),
    path('registro',registro,name='registro'),
    path('home',home, name='home'),
    path('logout',logout,name='logout'),
    path('admin/', admin.site.urls),
    path('recuperacion',recuperacion,name='recuperacion')
]