from django.db import models

# Create your models here .

class usuario(models.Model):
    rut = models.CharField(max_length=12, primary_key= True)
    nombreuser = models.CharField(max_length=20)
    nombre = models.CharField(max_length=40)
    apellido = models.CharField(max_length=40) 
    contraseña = models.CharField(max_length=30)
    email = models.EmailField(max_length = 254)
    fechaNac = models.DateField(max_length=8)   

class tipo_usuario(models.Model):
    id_tipo = models.IntegerField(primary_key= True)
    nombre_tipo = models.CharField(max_length =20)

class medalla(models.Model):
    id_medalla = models.IntegerField(primary_key=True)
    imagen = models.ImageField(max_length=8)
    puntaje = models.IntegerField(max_length=8)
    titulo = models.CharField(max_length=20)

class juego(models.Model):
    id_juego = models.IntegerField(primary_key=True)
    puntuacion = models.IntegerField(max_length=8)
    id_rango = models.IntegerField(max_length=8)
    rut = models.CharField(max_length =10)
    Fecha_juego = models.DateField(max_length=8)

class usuario_medalla(models.Model):
    id = models.IntegerField(primary_key=True)
    rut = models.CharField(max_length= 12)
    id_medalla = models.IntegerField(max_length=8)
    fecha_obt = models.DateField(max_length=8)
    
class rangos(models.Model):
    id_rango = models.IntegerField(primary_key=True)
    minimo = models.IntegerField(max_length=8)
    maximo = models.IntegerField(max_length=8)
    dificultad = models.CharField(max_length=8)