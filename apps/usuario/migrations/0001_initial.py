# Generated by Django 2.2.3 on 2019-10-02 15:39

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='apoderado',
            fields=[
                ('RutApoderado', models.CharField(max_length=12, primary_key=True, serialize=False)),
                ('Nombre', models.CharField(max_length=40)),
                ('Apellido', models.CharField(max_length=40)),
                ('Direccion', models.CharField(max_length=200)),
                ('RutNiño', models.CharField(max_length=12)),
            ],
        ),
    ]
