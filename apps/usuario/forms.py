from django import forms
from .models import usuario
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

#login
from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login as do_login



class Registrar(UserCreationForm):
    username = forms.CharField(label='Nickname')
    first_name = forms.CharField(label='Nombre')
    last_name= forms.CharField(label='Apellido')
    email= forms.EmailField(label='E-mail',widget=forms.EmailInput())
    class Meta:
        model = User
        fields = ('username','first_name','last_name','email')


class loguear(AuthenticationForm):
    username = forms.CharField(required=True,label='Nickname')
    password = forms.CharField(required= True,label='Contraseña',widget=forms.PasswordInput())
    class Meta:
        model = User
        fields = ('username','password')


class recuForm(forms.Form):
    email = forms.EmailField(required=True,label= 'Correo')
    class Meta:
        model = User
        field = ('email')
