# logout
from django.shortcuts import render, redirect
from django.contrib.auth import logout as do_logout

#login
from django.contrib.auth import authenticate
from django.contrib.auth import login as do_login
from .forms import loguear
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm

#recuperacion
from .forms import recuForm

#registro
from django.contrib.auth.forms import UserCreationForm
from .forms import Registrar
from .models import usuario


# Create your views here.

def welcome(request):
    # Si estamos identificados devolvemos la portada
    if request.user.is_authenticated:
        return render(request, "usuario/logueado/welcome.html")
    # En otro caso redireccionamos al login
    return redirect('/login')

def login(request):
    # Creamos el formulario de autenticación vacío
    form = loguear(AuthenticationForm)
    if request.method == "POST":
        # Añadimos los datos recibidos al formulario
        form = AuthenticationForm(data=request.POST)
        # Si el formulario es válido...
        if form.is_valid():
            # Recuperamos las credenciales validadas
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            # Verificamos las credenciales del usuario
            user = authenticate(username=username, password=password)

            # Si existe un usuario con ese nombre y contraseña
            if user is not None:
                # Hacemos el login manualmente
                do_login(request, user)
                # Y le redireccionamos a la portada
                return redirect('home')

    # Si llegamos al final renderizamos el formulario
    
    return render(request,'usuario/login.html',{'form': form})

def registro(request):
    form = Registrar()
    if request.method == "POST":
        form = Registrar(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('login')
    else:
        form = Registrar()
    form.fields['username'].help_text = None
    form.fields['password1'].help_text = None
    form.fields['password2'].help_text = None
    return render(request, 'usuario/registro.html',{'form':form})

def home(request):
    return render(request,'usuario/index.html')

def logout(request):
    # Finalizamos la sesión
    do_logout(request)
    # Redireccionamos a la portada
    return redirect('home')

def recuperacion(request):
    form = recuForm()
  
    return render(request, 'recuperacion/password_reset_form.html',{'form':form} )


